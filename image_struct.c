#include <stdint.h>
#include <stddef.h>
#include "image_struct.h"

struct pixel* get_pixel(const struct image img, uint64_t x, uint64_t y) {
	return &img.data[y*img.width + x];
}

uint32_t get_width(struct image* image){
	return image->width;
}

uint32_t get_height(struct image* image){
	return image->height;
}

unsigned char get_blue(struct pixel* pixel) {
	return pixel->b;
}

unsigned char get_red(struct pixel* pixel) {
	return pixel->r;
}

unsigned char get_green(struct pixel* pixel) {
	return pixel->g;
}
