enum read_status {
	READ_OK = 0,
	READ_HEADER_NOT_POSSIBLE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER
};

enum write_status {
	WRITE_OK = 0,
	WRITE_ERROR_HEADER,
	WRITE_ERROR_BIT_MAP
};
