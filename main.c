#include "bmp_utils.h"
#include <sys/resource.h>
#include "image_sepia.h"
#include "sepia_sse.h"
#include <stdlib.h>
#include <string.h>

void check_function(void (*function) (struct image*), struct image* image, char* type);

void check_sepia(char* filename, char* type);

int main(int argc, char* argv[]){
	
	check_sepia(argv[1], argv[2]);
	return 0;
}

void check_sepia(char* filename, char* type){
	FILE* file = fopen(filename, "rb");
	if (file == NULL){
		printf("Couldn't open file\n");
		return;
	}else{
		char* filename;
		struct image* map = malloc(sizeof(struct image));
		char* message = get_message_read(from_bmp(file, map));
		if (strcmp(message,"Successful reading!") == 0){
			printf("-----------------------------\n");
			if (strcmp(type, "C") == 0){
				check_function(sepia_c, map, type);
				filename = "test_sepia_c.bmp";
			}else if (strcmp(type, "ASM") == 0 ){
				filename = "test_sepia_asm.bmp";
				check_function(sepia_sse, map, type);
			}
			fclose(file);
			file = fopen(filename, "wb");
			message = get_message_write(write_bmp(map, file, 0x4D42));
			printf("%s\n", message);
		}else{
			printf("%s\n", message);
		}
		free(map);
	}
	fclose(file);
}

void check_function(void (*function) (struct image*), struct image* image, char* type){
	struct rusage rusage;
	struct timeval start;
	struct timeval end;
	long res;
	getrusage(RUSAGE_SELF, &rusage);
	start = rusage.ru_utime;
	function(image);
	getrusage(RUSAGE_SELF, &rusage);
	end = rusage.ru_utime;
	res = ((end.tv_sec - start.tv_sec) * 1000L) + (end.tv_usec - start.tv_usec)/1000;
	printf( "Time elapsed in microseconds : %ld in %s\n", res, type);
	printf("-----------------------------\n");
}	
