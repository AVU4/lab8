#include "sepia_sse.h"
#include <stdio.h>
#include "image_sepia.h"

extern void sepia_asm(float src[12], float res[12]);

void sepia_sse(struct image* image){
	size_t width = image->width;
	size_t height = image->height;
	size_t pixel_size = width * height;
	size_t fin_size = (pixel_size / 4)*4;
	struct pixel pixel;
	float src[12];
	float res[12];
	size_t i;
	size_t rest;
	for (i = 0; i < fin_size; i += 4){
		size_t j;
		for (j = 0; j < 4; j ++) {
			pixel = image->data[i + j];
			src[j*3] = pixel.r;
			src[j*3 + 1] = pixel.g;
			src[j*3 + 2] = pixel.b;
		}
		sepia_asm(src, res);
		for (j = 0; j < 4; j ++) {
			pixel.r = res[j*3];
		       	pixel.g = res[j*3 + 1];
		       	pixel.b = res[j*3 + 2];
			image->data[i + j] = pixel;
		}
	}
	
	rest = pixel_size - fin_size;
	for (i = 0; i < rest; i ++) {
		pixel = image->data[i + fin_size];
		sepia_one(&pixel);
	}

}
