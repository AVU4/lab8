#include "image_sepia.h"

static unsigned char sat(uint64_t x) {
	if (x < 256) return x;
	return 255;
}

void sepia_one(struct pixel* pixel) {	
	static const float c[3][3] = {
		{ 0.393f, 0.349f, 0.272f },
		{ 0.769f, 0.686f, 0.534f },
		{ 0.189f, 0.168f, 0.131f }
	};

	struct pixel* old = pixel;

	pixel->r = sat(
			old->r * c[0][0] + old->g * c[1][0] + old->b * c[2][0]
		      );

	pixel->g = sat(
			old->r * c[0][1] + old->g * c[1][1] + old->b * c[2][1]
		      );
	pixel->b = sat(
			old->r * c[0][2] + old->g * c[1][2] + old->b * c[2][2]
		      );
}

void sepia_c(struct image* img) {
	size_t x, y;
	for (y = 0; y < img->height; y ++)
		for ( x = 0; x < img->width; x ++)
			sepia_one(get_pixel(*img, x, y) );
}
