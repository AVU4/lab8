C_FLAGS = -march=x86-64 -O0 -ansi -pedantic -Wall -Werror  -lm -g -c
ASM_FLAGS = -felf64 -o
compile:
	gcc $(C_FLAGS) image_struct.c -o build/image_struct.o
	gcc $(C_FLAGS) bmp_utils.c -o build/bmp_utils.o
	gcc $(C_FLAGS) image_sepia.c -o build/image_sepia.o
	nasm $(ASM_FLAGS) build/sepia.o sepia.asm
	gcc $(C_FLAGS) sepia_sse.c -o build/sepia_sse.o
	gcc $(C_FLAGS) main.c -o build/main.o
	gcc -g -lm build/*.o -o build/main
	rm -rf build/*.o
run_first:
	build/./main 9.bmp ASM
	build/./main 9.bmp C
run_second:
	build/./main MARBLES.BMP C
	build/./main MARBLES.BMP ASM

debug:
	gdb build/main

delete:
	rm test_sepia_*
