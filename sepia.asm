default rel

%define c11 0.393
%define c12 0.769
%define c13 0.189
%define c21 0.349
%define c22 0.686
%define c23 0.168
%define c31 0.272
%define c32 0.534
%define c33 0.131

%macro fill_part_1 2
	mov eax, dword [rdi + 4*%2]
	mov dword [rsp], eax
	mov dword [rsp + 4], eax
	mov dword [rsp + 8], eax
	mov eax, dword [rdi + 12 + 4*%2]
	mov dword [rsp + 12], eax
	movups %1, [rsp]
%endmacro

%macro fill_part_2 2
	mov eax, dword [rdi + 4*%2]
	mov dword [rsp], eax
	mov dword [rsp + 4], eax
	mov eax, dword [rdi + 12 + 4*%2]
	mov dword [rsp + 8], eax
	mov dword [rsp + 12], eax
	movups %1, [rsp]
%endmacro

%macro fill_part_3 2
	mov eax, dword [rdi + 4*%2]
	mov dword [rsp], eax
	mov eax, dword [rdi + 12 + 4*%2]
	mov dword [rsp + 4], eax
	mov dword [rsp + 8], eax
	mov dword [rsp + 12], eax
	movups %1, [rsp]
%endmacro

%macro compute 1
	movups xmm3, [r_coefs + 4*%1]
	movups xmm4, [g_coefs + 4*%1]
	movups xmm5, [b_coefs + 4*%1]

	mulps xmm0, xmm3
	mulps xmm1, xmm4
	mulps xmm2, xmm5

	addps xmm0, xmm1
	addps xmm0, xmm2
	
	
	cvtps2dq xmm0, xmm0 ;convert  float to  int32
	packusdw xmm0, xmm0; convert  int32 to int16
	packuswb xmm0, xmm0; convert int16 to int8
	punpcklbw xmm0, xmm6; convert int8 to int16
	punpcklwd xmm0, xmm6; convert int16 to int32
	cvtdq2ps xmm0, xmm0; convert int32 to float

	movups [rsi + 16*%1], xmm0

%endmacro
global sepia_asm


section .rodata
	r_coefs : dd c11, c21, c31, c11, c21, c31, c11, c21, c31, c11
	g_coefs : dd c12, c22, c32, c12, c22, c32, c12, c22, c32, c12
	b_coefs : dd c13, c23, c33, c13, c23, c33, c13, c23, c33, c13
section .text

sepia_asm:
	pxor xmm6, xmm6
	sub rsp, 16
	
	fill_part_1 xmm0, 0
	fill_part_1 xmm1, 1
	fill_part_1 xmm2, 2

	compute 0
	
	fill_part_2 xmm0, 3	
	fill_part_2 xmm1, 4
	fill_part_2 xmm2, 5
	
	compute 1

	fill_part_3 xmm0, 6
	fill_part_3 xmm1, 7
	fill_part_3 xmm2, 8
	
	compute 2	

	add rsp, 16
	ret 
