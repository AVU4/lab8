#ifndef IMAGE_SEPIA_H
#define IMAGE_SEPIA_H
#include "image_struct.h"
#include <stddef.h>

void sepia_one(struct pixel* pixel);
void sepia_c(struct image* img);
#endif
