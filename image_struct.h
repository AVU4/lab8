#ifndef IMAGE_STRUCT_H
#define IMAGE_STRUCT_H
#include <stdint.h>

struct pixel {
	volatile unsigned char b, r, g;
};

struct image {
	volatile uint32_t width;
	volatile uint32_t height;
	struct pixel* data;
};

unsigned char get_blue(struct pixel* pixel);
unsigned char get_red(struct pixel* pixel);
unsigned char get_green(struct pixel* pixel);
uint32_t get_width(struct image* image);
uint32_t get_height(struct image* image);
struct pixel* get_pixel(const struct image img, uint64_t x, uint64_t y);
#endif
