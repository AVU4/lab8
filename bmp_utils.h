#ifndef LAB8_BMP_UTILS_H
#define LAB8_BMP_UTILS_H
#include <stdio.h>
#include <stdint.h>
#include "status.h"
#include "image_struct.h"
#include "bmp_struct.h"
enum read_status from_bmp(FILE* file, struct image*  data);
struct image* rotate(struct image*  in);
enum write_status write_bmp(struct image* in, FILE* file, uint16_t format);
void show(struct image* in);
char* get_message_read(enum read_status message);
char* get_message_write(enum write_status message);
#endif
